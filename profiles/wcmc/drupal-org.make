api = 2
core = 7.x

; The Panopoly Foundation

projects[panopoly_core][version] = 1.6
projects[panopoly_core][subdir] = panopoly

projects[panopoly_images][version] = 1.6
projects[panopoly_images][subdir] = panopoly

projects[panopoly_theme][version] = 1.6
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.6
projects[panopoly_magic][subdir] = panopoly

projects[panopoly_widgets][version] = 1.6
projects[panopoly_widgets][subdir] = panopoly

projects[panopoly_admin][version] = 1.6
projects[panopoly_admin][subdir] = panopoly

projects[panopoly_users][version] = 1.6
projects[panopoly_users][subdir] = panopoly

; The Panopoly Toolset

projects[panopoly_pages][version] = 1.6
projects[panopoly_pages][subdir] = panopoly

projects[panopoly_wysiwyg][version] = 1.6
projects[panopoly_wysiwyg][subdir] = panopoly

; Exclude Panopoly Search

;projects[panopoly_search][version] = 1.6
;projects[panopoly_search][subdir] = panopoly

; Analytics

projects[google_analytics][version] = 1.6
projects[google_analytics][subdir] = contrib

; SEO

projects[globalredirect][version] = 1.5
projects[globalredirect][subdir] = contrib

projects[redirect][version] = 1.0-rc1
projects[redirect][subdir] = contrib

projects[metatag][version] = 1.0-beta9
projects[metatag][subdir] = contrib

projects[pathologic][version] = 2.12
projects[pathologic][subdir] = contrib

; Search

projects[google_appliance][version] = 1.11
projects[google_appliance][subdir] = contrib

; Email

projects[smtp][version] = 1.0
projects[smtp][subdir] = contrib

; Webform

projects[webform][version] = 4.0-beta3
projects[webform][subdir] = contrib

<?php
/**
 * @file
 * Panopoly Admin overrides
 */

/**
 * Implements hook_strongarm_alter()
 */
if (!function_exists('panopoly_admin_strongarm_alter')) {
  function panopoly_admin_strongarm_alter(&$data) {
    // Change default admin role
    if (isset($data['user_admin_role'])) {
      $data['user_admin_role']->value = '5';
    }
  }
}

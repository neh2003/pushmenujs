<?php
/**
 * @file
 * Panopoly Pages overrides
 */

/**
 * Implements hook_field_default_field_bases().
 */
if (!function_exists('panopoly_pages_field_default_field_bases_alter')) {
function panopoly_pages_field_default_field_bases_alter(&$data) {
  // Exported field_base: 'field_related_articles'
  $data['field_related_articles'] = array(
    'active' => 1,
    'cardinality' => -1,
    'deleted' => 0,
    'entity_types' => array(),
    'field_name' => 'field_related_articles',
    'foreign keys' => array(
      'node' => array(
        'columns' => array(
          'target_id' => 'nid',
        ),
        'table' => 'node',
      ),
    ),
    'indexes' => array(
      'target_id' => array(
        0 => 'target_id',
      ),
    ),
    'locked' => 0,
    'module' => 'entityreference',
    'settings' => array(
      'handler' => 'base',
      'handler_settings' => array(
        'behaviors' => array(
          'views-select-list' => array(
            'status' => 0,
          ),
        ),
        'sort' => array(
          'type' => 'none',
        ),
        'target_bundles' => array(),
      ),
      'target_type' => 'node',
    ),
    'translatable' => 0,
    'type' => 'entityreference',
  );
  //dpm($data);
  }
}

/**
 * Implements hook_field_default_field_instances_alter()
 */
if (!function_exists('panopoly_pages_field_default_field_instances_alter')) {
  function panopoly_pages_field_default_field_instances_alter(&$data) {
    // Remove Featured Categories
    if (isset($data['node-panopoly_page-field_featured_categories'])) {
      unset($data['node-panopoly_page-field_featured_categories']);
    }
    // Expose Title field to be used as a caption for the hero image
    if (isset($data['node-panopoly_page-field_featured_image'])) {
      $data['node-panopoly_page-field_featured_image']['settings']['title_field'] = 1;
    }

    // Add the Related Articles custom field
    // Exported field_instance: 'node-panopoly_page-field_related_article'
    $data['node-panopoly_page-field_related_articles'] = array(
      'bundle' => 'panopoly_page',
      'default_value' => NULL,
      'deleted' => 0,
      'description' => '',
      'display' => array(
        'default' => array(
          'label' => 'above',
          'module' => 'entityreference',
          'settings' => array(
            'link' => FALSE,
          ),
          'type' => 'entityreference_label',
          'weight' => 4,
        ),
        'featured' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'search_result' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
        'teaser' => array(
          'label' => 'above',
          'settings' => array(),
          'type' => 'hidden',
          'weight' => 0,
        ),
      ),
      'entity_type' => 'node',
      'field_name' => 'field_related_articles',
      'label' => 'Related Articles',
      'required' => 0,
      'settings' => array(
        'user_register_form' => FALSE,
      ),
      'widget' => array(
        'active' => 1,
        'module' => 'entityreference',
        'settings' => array(
          'match_operator' => 'CONTAINS',
          'path' => '',
          'size' => 60,
        ),
        'type' => 'entityreference_autocomplete',
        'weight' => 41,
      ),
    );
  }
}

/**
 * Implements hook_panelizer_defaults_override_alter()
 */
if (!function_exists('panopoly_pages_panelizer_defaults_override_alter')) {
  function panopoly_pages_panelizer_defaults_override_alter(&$data) {

    /* Example: Rebuild the panelizer defaults for panopoly_page
    //
    // I think generally you don't want to completely rebuild objects like this
    //*/
    if (isset($data['node:panopoly_page:default'])) {
      // Wipe old defaults
      unset($data['node:panopoly_page:default']);

      // Rebuild node:panopoly_page:default
      $panelizer = new stdClass();
      $panelizer->disabled = FALSE;
      $panelizer->api_version = 1;
      $panelizer->name = 'node:panopoly_page:default';
      $panelizer->title = 'Default';
      $panelizer->panelizer_type = 'node';
      $panelizer->panelizer_key = 'panopoly_page';
      $panelizer->no_blocks = FALSE;
      $panelizer->css_id = '';
      $panelizer->css = '';
      $panelizer->pipeline = 'ipe';
      $panelizer->contexts = array();
      $panelizer->relationships = array();
      $panelizer->access = '';
      $panelizer->view_mode = 'page_manager';
      $panelizer->css_class = '';
      $panelizer->title_element = 'H2';
      $panelizer->link_to_entity = TRUE;
      $panelizer->extra = '';
      $display = new panels_display();
      $display->layout = 'wcmc_hero_threecol';
      $display->layout_settings = array();
      $display->panel_settings = array(
        'hero' => NULL,
        'main_content' => NULL,
        'related_content_sidebar' => NULL,
        'information_sidebar' => NULL,
      );
      $display->cache = array();
      $display->title = '%node:title';
      $display->content = array();
      $display->panels = array();
        $pane = new stdClass();
        $pane->pid = 'new-1';
        $pane->panel = 'hero';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:field_featured_image';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'hidden',
          'formatter' => 'image',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(
            'image_link' => '',
            'image_style' => '',
          ),
          'context' => 'panelizer',
          'override_title' => 0,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 0;
        $pane->locks = array();
        $display->content['new-1'] = $pane;
        $display->panels['hero'][0] = 'new-1';

        $pane = new stdClass();
        $pane->pid = 'new-2';
        $pane->panel = 'main_content';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:body';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'hidden',
          'formatter' => 'text_default',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(
            'trim_length' => '250',
          ),
          'context' => 'panelizer',
          'override_title' => 0,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 1;
        $pane->locks = array();
        $display->content['new-2'] = $pane;
        $display->panels['main_content'][0] = 'new-2';

        $pane = new stdClass();
        $pane->pid = 'new-3';
        $pane->panel = 'related_content_sidebar';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:field_related_articles';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'title',
          'formatter' => 'entityreference_label',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(
            'view_mode' => 'full',
            'links' => 1,
            'link' => 1,
          ),
          'context' => 'panelizer',
          'override_title' => 0,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 1;
        $pane->locks = array();
        $display->content['new-3'] = $pane;
        $display->panels['related_content_sidebar'][0] = 'new-3';

      $display->hide_title = PANELS_TITLE_FIXED;
      $display->title_pane = 'new-1';
      $panelizer->display = $display;
      $data['node:panopoly_page:default'] = $panelizer;
    }

    // Rebuild node:panopoly_page:default:default
    if (isset($data['node:panopoly_page:default:default'])) {
      // Wipe old defaults
      unset($data['node:panopoly_page:default:default']);

      // Rebuild node:panopoly_page:default
      $panelizer = new stdClass();
      $panelizer->disabled = FALSE;
      $panelizer->api_version = 1;
      $panelizer->name = 'node:panopoly_page:default:default';
      $panelizer->title = 'Default';
      $panelizer->panelizer_type = 'node';
      $panelizer->panelizer_key = 'panopoly_page';
      $panelizer->no_blocks = FALSE;
      $panelizer->css_id = '';
      $panelizer->css = '';
      $panelizer->pipeline = 'ipe';
      $panelizer->contexts = array();
      $panelizer->relationships = array();
      $panelizer->access = array();
      $panelizer->view_mode = 'default';
      $panelizer->css_class = 'page-default';
      $panelizer->title_element = 'H2';
      $panelizer->link_to_entity = TRUE;
      $panelizer->extra = '';
      $display = new panels_display();
      $display->layout = 'wcmc_hero_threecol';
      $display->layout_settings = array();
      $display->panel_settings = array(
        'style_settings' => array(
          'hero' => NULL,
          'main_content' => NULL,
          'related_content_sidebar' => NULL,
          'information_sidebar' => NULL,
        ),
      );
      $display->cache = array();
      $display->title = '%node:title';
      $display->content = array();
      $display->panels = array();
        $pane = new stdClass();
        $pane->pid = 'new-7';
        $pane->panel = 'hero';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:field_featured_image';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'hidden',
          'formatter' => 'image',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(
            'image_style' => '',
            'image_link' => '',
          ),
          'context' => 'panelizer',
          'override_title' => 1,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 0;
        $pane->locks = array();
        $display->content['new-7'] = $pane;
        $display->panels['hero'][0] = 'new-7';

        $pane = new stdClass();
        $pane->pid = 'new-8';
        $pane->panel = 'main_content';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:body';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'hidden',
          'formatter' => 'text_default',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(),
          'context' => 'panelizer',
          'override_title' => 1,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 1;
        $pane->locks = array();
        $display->content['new-8'] = $pane;
        $display->panels['main_content'][0] = 'new-8';

        $pane = new stdClass();
        $pane->pid = 'new-9';
        $pane->panel = 'related_content_sidebar';
        $pane->type = 'entity_field';
        $pane->subtype = 'node:field_related_articles';
        $pane->shown = TRUE;
        $pane->access = array();
        $pane->configuration = array(
          'label' => 'title',
          'formatter' => 'entityreference_label',
          'delta_limit' => 0,
          'delta_offset' => '0',
          'delta_reversed' => FALSE,
          'formatter_settings' => array(
            'view_mode' => 'full',
            'links' => 1,
            'link' => 1,
          ),
          'context' => 'panelizer',
          'override_title' => 0,
          'override_title_text' => '',
        );
        $pane->cache = array();
        $pane->style = array(
          'settings' => NULL,
        );
        $pane->css = array();
        $pane->extras = array();
        $pane->position = 1;
        $pane->locks = array();
        $display->content['new-9'] = $pane;
        $display->panels['related_content_sidebar'][0] = 'new-9';

      $display->hide_title = PANELS_TITLE_NONE;
      $display->title_pane = '0';
      $panelizer->display = $display;
      $data['node:panopoly_page:default:default'] = $panelizer;
    }
  }
}

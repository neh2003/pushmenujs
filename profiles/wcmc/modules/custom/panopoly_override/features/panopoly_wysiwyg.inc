<?php
/**
 * @file
 * Panopoly Wysiwyg overrides
 */

/**
 * Implements hook_filter_default_formats_alter()
 */
if (!function_exists('panopoly_wysiwyg_filter_default_formats_alter')) {
  function panopoly_wysiwyg_filter_default_formats_alter(&$data) {
    $data['panopoly_wysiwyg_text']['filters']['wysiwyg']['settings']['valid_elements'] = '
    a[!href|target<_blank|title|class|style],
    div[style|class|align<center?justify?left?right],
    br,em,strong,cite,code,blockquote,ul,ol,li,dl,dt,dd,
    i[class],
    span[style],p[style|class],
    h1,h2,h3,h4,h5,h6,
    img[!src|title|alt|style|width|height|class|hspace|vspace],
    @[style]
    iframe[*]';

    $data['panopoly_html_text']['filters']['wysiwyg']['settings']['valid_elements'] = '
    a[!href|target<_blank|title|class|style],
    div[style|class|align<center?justify?left?right],
    br,em,strong,cite,code,blockquote,ul,ol,li,dl,dt,dd,
    i[class],
    span[style],p[style|class],
    h1,h2,h3,h4,h5,h6,
    img[!src|title|alt|style|width|height|class|hspace|vspace],
    @[style]
    iframe[*]';
  }
}


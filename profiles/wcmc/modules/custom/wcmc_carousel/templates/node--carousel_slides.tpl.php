<?php
  //Get all the slide data needed for display
  $field_overlay_text = field_get_items('node', $node, 'field_overlay_text');
  $overlay_text = render(field_view_value('node', $node, 'field_overlay_text', $field_overlay_text[0]));

  $field_carousel_image= field_get_items('node', $node, 'field_carousel_image');
  $carousel_image = render(field_view_value('node', $node, 'field_carousel_image', $field_carousel_image[0]));

  $field_sub_text = field_get_items('node', $node, 'field_sub_text');
  $sub_text = render(field_view_value('node', $node, 'field_sub_text', $field_sub_text[0]));

  $field_carousel_link = field_get_items('node', $node, 'field_carousel_link');
  $carousel_link = $field_carousel_link[0]['display_url'];
?>

<div class="carousel-slide">
  <div class="slide-image">
    <p class="overlay-text"><?php print $overlay_text; ?></p>
    <?php if ($carousel_link != '') { ?>
      <a href="<?php print $carousel_link; ?>">
    <?php } ?>
        <?php print $carousel_image; ?>
    <?php if ($carousel_link != '') { ?>
      </a>
    <?php } ?>
  </div>
  <div class="slide-text">
    <div class="slide-caption-wrap">
      <div class="slide-title-wrap">
        <h2 class="slide-title">
          <?php if ($carousel_link != '') { ?>
            <a href="<?php print $carousel_link; ?>">
          <?php } ?>
            <?php print $title; ?>
          <?php if ($carousel_link != '') { ?>
            </a>
          <?php } ?>
        </h2>
      </div>
      <?php print $sub_text; ?>
    </div>
  </div>
</div>

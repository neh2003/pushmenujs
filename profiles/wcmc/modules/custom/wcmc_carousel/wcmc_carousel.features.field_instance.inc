<?php
/**
 * @file
 * wcmc_carousel.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function wcmc_carousel_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance: 'node-carousel_slides-field_carousel_image'
  $field_instances['node-carousel_slides-field_carousel_image'] = array(
    'bundle' => 'carousel_slides',
    'deleted' => 0,
    'description' => 'Upload an image to be displayed in the carousel on the homepage.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'image',
        'settings' => array(
          'image_link' => '',
          'image_style' => '',
        ),
        'type' => 'image',
        'weight' => 1,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_carousel_image',
    'label' => 'Carousel Image',
    'required' => 0,
    'settings' => array(
      'alt_field' => 1,
      'default_image' => 0,
      'file_directory' => 'carousel_slides',
      'file_extensions' => 'png gif jpg jpeg',
      'max_filesize' => '',
      'max_resolution' => '',
      'min_resolution' => '',
      'title_field' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'image',
      'settings' => array(
        'preview_image_style' => 'panopoly_image_half',
        'progress_indicator' => 'throbber',
      ),
      'type' => 'image_image',
      'weight' => 2,
    ),
  );

  // Exported field_instance: 'node-carousel_slides-field_sub_text'
  $field_instances['node-carousel_slides-field_sub_text'] = array(
    'bundle' => 'carousel_slides',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'A brief paragraph to be displayed with the slide.',
    'display' => array(
      'default' => array(
        'label' => 'hidden',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 0,
      ),
      'featured' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
      'teaser' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 0,
      ),
    ),
    'entity_type' => 'node',
    'field_name' => 'field_sub_text',
    'label' => 'Sub-text',
    'required' => 0,
    'settings' => array(
      'linkit' => array(
        'enable' => 0,
        'insert_plugin' => '',
      ),
      'text_processing' => 1,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'rows' => 3,
      ),
      'type' => 'text_textarea',
      'weight' => 3,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('A brief paragraph to be displayed with the slide.');
  t('Carousel Image');
  t('Sub-text');
  t('Upload an image to be displayed in the carousel on the homepage.');

  return $field_instances;
}

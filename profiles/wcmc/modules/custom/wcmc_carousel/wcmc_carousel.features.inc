<?php
/**
 * @file
 * wcmc_carousel.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wcmc_carousel_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wcmc_carousel_views_api() {
  return array(
    'api' => 3,
    'path' => drupal_get_path('module', 'wcmc_carousel'),
    'template path' => drupal_get_path('module', 'wcmc_carousel') . '/templates',
  );
}

/**
 * Implements hook_node_info().
 */
function wcmc_carousel_node_info() {
  $items = array(
    'carousel_slides' => array(
      'name' => t('Carousel Slides'),
      'base' => 'node_content',
      'description' => t('Add a slide to be featured in the carousel on the homepage.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

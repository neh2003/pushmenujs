<?php
/**
 * @file
 * wcmc_carousel.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wcmc_carousel_user_default_permissions() {
  $permissions = array();

  // Exported permission: create carousel_slides content.
  $permissions['create carousel_slides content'] = array(
    'name' => 'create carousel_slides content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any carousel_slides content.
  $permissions['delete any carousel_slides content'] = array(
    'name' => 'delete any carousel_slides content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own carousel_slides content.
  $permissions['delete own carousel_slides content'] = array(
    'name' => 'delete own carousel_slides content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any carousel_slides content.
  $permissions['edit any carousel_slides content'] = array(
    'name' => 'edit any carousel_slides content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own carousel_slides content.
  $permissions['edit own carousel_slides content'] = array(
    'name' => 'edit own carousel_slides content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  return $permissions;
}

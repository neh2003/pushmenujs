<?php
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('AddThis'),
  'description' => t('Displays AddThis Content Sharing Links'),
  'category' => t('WCMC'),
  'single' => TRUE,
  'content_types' => array('wcmc_addthis_content_type'),
  'admin info' => 'wcmc_addthis_content_type_admin_info',
  'render callback' => 'wcmc_addthis_content_type_render',
  'edit form' => 'wcmc_addthis_content_type_edit_form',
);

/**
* Output function for the POPS Profile client.
*/
function wcmc_addthis_content_type_render($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();

  if (!empty($conf['style_settings']) == 0) {
    $style_settings = 'addthis_32x32_style';
  } else {
    $style_settings = 'addthis_16x16_style';
  }

  $block->content = theme('wcmc_addthis', array(
    'style_settings' => $style_settings,
  ));
  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function wcmc_addthis_content_type_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'Social Sharing Icons';
    $block->content = '';
    return $block;
  }
}

/**
 * 'Edit form' callback for the content type.
 *
 */
function wcmc_addthis_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];
  $form['style_settings'] = array(
    '#type' => 'select',
    '#title' => t('Social Icons Setting'),
    '#options' => array(
      0 => t('Large Icons'),
      1 => t('Small Icons'),
    ),
    '#default_value' => $conf['style_settings'],
    '#description' => t('Choose whether you want large or small Social Icons.'),
  );
  return $form;
}

function wcmc_addthis_content_type_edit_form_submit($form, &$form_state) {
  foreach (array('style_settings') as $key) {
    $form_state['conf'][$key] = $form_state['values'][$key];
  }
}
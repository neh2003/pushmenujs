<?php
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Contact Info Pane'),
  'description' => t('Display the site contact information'),
  'category' => t('WCMC'),
  'single' => TRUE,
  'admin info' => 'wcmc_contact_pane_content_type_admin_info',
  'render callback' => 'wcmc_contact_pane_content_type_render',
);

/**
* Output function for the POPS Profile client.
*/
function wcmc_contact_pane_content_type_render($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();

  $site_name = variable_get('site_name');
  $site_affiliation = variable_get('site_affiliation');
  $site_street_address = variable_get('site_street_address');
  $site_phone = variable_get('site_phone');
  $site_fax = variable_get('site_fax');
  $site_zip = variable_get('site_zip');
  $site_mail = variable_get('site_mail');

  $block->content = theme('wcmc_contact_pane', array(
    'site_name' => $site_name,
    'site_affiliation' => $site_affiliation,
    'site_street_address' => $site_street_address,
    'site_phone' => $site_phone,
    'site_fax' => $site_fax,
    'site_zip' => $site_zip,
    'site_mail' => $site_mail,
  ));
  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function wcmc_contact_pane_content_type_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'Contact Information';
    $block->content = '';
    return $block;
  }
}


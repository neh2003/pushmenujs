<?php
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Social Media Links'),
  'description' => t('Display links to social media services.'),
  'category' => t('WCMC'),
  'single' => TRUE,
  'admin info' => 'wcmc_social_media_content_type_admin_info',
  'render callback' => 'wcmc_social_media_content_type_render',
);

/**
* Output function for the POPS Profile client.
*/
function wcmc_social_media_content_type_render($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();

  $social_facebook = variable_get('social_facebook');
  $social_twitter = variable_get('social_twitter');
  $social_youtube = variable_get('social_youtube');

  $block->content = theme('wcmc_social_media', array(
    'social_facebook' => $social_facebook,
    'social_twitter' => $social_twitter,
    'social_youtube' => $social_youtube,
  ));

  return $block;
}

/**
 * 'admin info' callback for panel pane.
 */
function wcmc_social_media_content_type_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'Social Media Links';
    $block->content = '';
    return $block;
  }
}


<?php

/**
 * @file
 * WCMC Reveal Pane Style
 */

// Plugin definition
$plugin = array(
  'title' => t('Reveal'),
  'description' => t('Presents a pane title that reveals the contents when clicked'),
  'render pane' => 'reveal_style_render_pane',
  'pane settings form' => 'wcmc_custom_panes_reveal_style_settings_form',
);

/**
 * Render callback.
 *
 * Declared by name in the HOOK_panels_style() above.
 *
 */
function theme_reveal_style_render_pane($vars) {

  $settings = $vars['settings'];
  $content = $vars['content'];

  $start_settings = $settings['collapsed']; // we can do this be cause the only values possible are 0 or 1

  $pane_content = render($content->content);

  if ($content->title) {
    $pane_title = '<h2 class="pane-title">'. $content->title .'</h2>';

    // Using the collapsible theme function from CTools
    // theme('ctools_collapsible', $handle, $content, $collapsed);

    $result =
      theme('ctools_collapsible',
        array(
          'handle' => $pane_title,
          'content' => $pane_content,
          'collapsed' => $start_settings,
        )
      );
  }

  // if we don't have a pane title, we just print out the content normally, since there's no handle
  else {
    $result = $pane_content;
  }

  return $result;
}

/**
* Settings form callback.
*/
function wcmc_custom_panes_reveal_style_settings_form($style_settings) {
  $form['collapsed'] = array(
    '#type' => 'select',
    '#title' => t('Reveal Setting'),
    '#options' => array(
      0 => t('Start revealed'),
      1 => t('Start hidden'),
    ),
    '#default_value' => (isset($style_settings['collapsed'])) ? $style_settings['collapsed'] : 1,
    '#description' => t('Choose whether you want the pane to start collapsed or revealed'),
  );

  return $form;
}
<?php
/**
 * @file
 * Display output for WCMC Contact Pane.
 *
 */
?>

 <div class="contact-info">
  <h3 class="pane-title">Contact Information</h3>
   <p>
   <span class="site-name"><?php print $site_name; ?></span>
   <span class="site-affiliation"><?php print $site_affiliation; ?></span>
   <span class="address"><?php print $site_street_address;?></span>
   <span class="city-state">New York, NY <?php print $site_zip; ?></span>
   <span class="phone">Phone: <?php print $site_phone; ?></span>
   <?php if ($site_fax != '') { ?>
    <span class="fax">Fax: <?php print $site_fax; ?></span>
   <?php } ?>
   </p>
 </div>





<?php
/**
 * @file
 * Display output for WCMC Social Media Pane.
 *
 */
?>

  <div class="social-media"><h3>Follow Us</h3>
    <ul class="social-links">
      <li><a class="wcmc-social-button-facebook ss-icon ss-social-regular" href="<?php print $social_facebook; ?>">Facebook</a></li>
      <li><a class="wcmc-social-button ss-icon ss-social-regular" href="<?php print $social_twitter; ?>">Twitter</a></li>
      <li><a class="wcmc-social-button ss-icon ss-social-regular" href="<?php print $social_youtube; ?>">youtube</a></li>
    </ul></div>
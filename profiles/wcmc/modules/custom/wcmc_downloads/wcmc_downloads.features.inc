<?php
/**
 * @file
 * wcmc_downloads.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wcmc_downloads_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wcmc_downloads_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wcmc_downloads_node_info() {
  $items = array(
    'downloads' => array(
      'name' => t('Downloads'),
      'base' => 'node_content',
      'description' => t('Make a form or another file available for download.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

(function($) {
  Drupal.behaviors.bef_live_filter = {
    attach: function(context, settings) {
      // Hide the apply button.
      $('.views-exposed-form input:submit', context).hide();

      // When the change event fires, run the submit handler
      $('.views-exposed-form input, .views-exposed-form select', context).change(function(event) {
        $(this).parents('form').find('.views-exposed-form input:submit').click();
      });
    }
  }
})(jQuery);
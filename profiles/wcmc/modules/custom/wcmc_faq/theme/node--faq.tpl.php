<?php

  //Get the value of the "Answer" field
  $answer_field = field_get_items('node', $node, 'body');
  $answer_markup = render(field_view_value('node', $node, 'body', $answer_field[0]));

  // Insert the "A." prefix into the markup
  $answer_display = substr($answer_markup, 0, 3) . 'A: ' . substr($answer_markup, 3);

?>

<div class="q-and-a">
  <p class="question">Q: <?php print $title; ?></p>
  <div class="answer"><?php print $answer_display; ?></div>
</div>
<?php
/**
 * @file
 * wcmc_faq.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wcmc_faq_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implementation of hook_views_api().
 */
function wcmc_faq_views_api() {
  return array(
    'api' => 3,
    'template path' => drupal_get_path('module', 'wcmc_faq') . '/theme',
  );
}

/**
 * Implements hook_node_info().
 */
function wcmc_faq_node_info() {
  $items = array(
    'faq' => array(
      'name' => t('FAQ'),
      'base' => 'node_content',
      'description' => t('Add a "frequently asked question" to the knowledge base.'),
      'has_title' => '1',
      'title_label' => t('Question'),
      'help' => '',
    ),
  );
  return $items;
}

(function($){
  $(document).ready(function() {
    $(".fancybox").fancybox({
      openEffect  : 'none',
      closeEffect : 'none',
      prevEffect  : 'none',
      nextEffect  : 'none',
      closeBtn    : false,
      nextClick   : true,
      helpers     : {
        title : { type : 'inside' },
        buttons : {}
      }
    });
    $(".fancybox-video").fancybox({
      openEffect  : 'none',
      closeEffect : 'none'
    });
    // View Slideshow link
    $(".trigger-slideshow").click(function() {
      $(".fancybox").eq(0).trigger('click');
    });
    $(".trigger-video").click(function() {
      $(".fancybox-video").eq(0).trigger('click');
    });
    $(".photo-icon, .playhead").show();
  });
})(jQuery);
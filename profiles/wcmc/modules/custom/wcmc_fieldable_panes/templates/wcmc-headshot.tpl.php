<?php
 // These variables are set in wcmc_fieldable_panes_preprocess_fieldable_panels_pane()
?>

<div class="headshot-entry">
  <?php if ($image != '') {

    $prep_image = array(
        'style_name' => 'headshot',
        'path' => $image['uri'],
        'width' => '',
        'height' => '',
        'alt' => $name,
      );

    print $headshot_image = theme('image_style', $prep_image);
  } else { ?>
    <img src="/<?php print $module_path; ?>/images/people-placeholder.png" alt="placholder" />
  <?php } ?>
  <div class="headshot-details">
    <p class="name"><?php print $name; ?></p>
    <p class="title"><?php print $prof_title_display; ?></p>
    <div class="description"><?php print $description; ?></div>
  </div>
</div>
<div class="slideshow-images">
<?php
$i = 0;
foreach ($items as $delta => $item) {
  $image_path = render(file_create_url($item['#item']['uri']));
  $image_title = $item['#item']['title'];

  // Add hidden class to all but first image
  if ($i == 0) { ?>
    <a href="<?php print $image_path; ?>" rel="slideshow" class="fancybox" title="<?php print $image_title; ?>">
  <?php } else { ?>
    <a href="<?php print $image_path; ?>" rel="slideshow" class="fancybox is-hidden" title="<?php print $image_title; ?>">
  <?php } ?>
      <?php print render($item); ?>
    </a>
<?php
  $i++;
} ?>
  <a class="trigger-slideshow photo-icon" href="#">Photos</a>
</div>



<a class="trigger-slideshow" href="#"><strong>View Slideshow:</strong></a>
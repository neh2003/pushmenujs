<?php
 // These variables are set in wcmc_fieldable_panes_preprocess_fieldable_panels_pane()
?>

<div class="tile">
  <a href="<?php print $tile_link; ?>"><img src="<?php print $image_src; ?>" alt="<?php print $image_alt; ?>" /></a>
  <a class="title" href="<?php print $tile_link; ?>"><?php print $image_title; ?></a>
</div>
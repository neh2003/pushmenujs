<?php
/**
 * @file
 * wcmc_fieldable_panes.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wcmc_fieldable_panes_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create fieldable headshot'.
  $permissions['create fieldable headshot'] = array(
    'name' => 'create fieldable headshot',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'super user' => 'super user',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'delete fieldable headshot'.
  $permissions['delete fieldable headshot'] = array(
    'name' => 'delete fieldable headshot',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'super user' => 'super user',
    ),
    'module' => 'fieldable_panels_panes',
  );

  // Exported permission: 'edit fieldable headshot'.
  $permissions['edit fieldable headshot'] = array(
    'name' => 'edit fieldable headshot',
    'roles' => array(
      'authenticated user' => 'authenticated user',
      'super user' => 'super user',
    ),
    'module' => 'fieldable_panels_panes',
  );

  return $permissions;
}

<?php
/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('WCMC Localist Pane'),
  'description' => t('This pane pulls in events from a Localist feed.'),
  'category' => t('Localist'),
  'single' => TRUE,
  'content_types' => array('wcmc_localist_pane_content_type'),
  'edit form' => 'wcmc_localist_pane_content_type_edit_form',
  'admin info' => 'wcmc_localist_pane_content_type_admin_info',
  'render callback' => 'wcmc_localist_pane_content_type_render',
  'defaults' => array(
    'localist_settings' => array(
    'localist_calendar_url' => 'http://events.weill.cornell.edu',
    'localist_display_quantity' => '10',
    'localist_days_ahead' => '365',
    'localist_sort_option' => 'date',
    'localist_filter_department' => '0',
    ),
  )
);

/**
 * 'Edit form' callback for the content type. Provides a form to capture
 *  configuration settings.
 */
function wcmc_localist_pane_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  // Localist Settings Fieldset
  $form['localist_settings'] = array(
    '#title' => t('Localist Settings'),
    '#type' => 'fieldset',
    '#collapsible' => FALSE,
    '#prefix' => '<div class="localist-settings">',
    '#suffix' => '</div>',
  );

  // Calendar URL field
  $form['localist_settings']['localist_calendar_url'] = array(
    '#title' => t('Localist Calendar URL'),
    '#description' => t('e.g. http://events.weill.cornell.edu'),
    '#type' => 'textfield',
    '#maxlength' => 64,
    '#size' => 64,
    '#default_value' => (!empty($conf['localist_settings']['localist_calendar_url']))
                        ? $conf['localist_settings']['localist_calendar_url']
                        : 'http://events.weill.cornell.edu',
    '#required' => TRUE,
  );

  // Display quantity
  $form['localist_settings']['localist_display_quantity'] = array(
    '#title' => t('Number of Events to Show'),
    '#description' => t('Maximum: 100'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 6,
    '#default_value' => (!empty($conf['localist_settings']['localist_display_quantity']))
                        ? $conf['localist_settings']['localist_display_quantity']
                        : '10',
  );

  // Days into the future
  $form['localist_settings']['localist_days_ahead'] = array(
    '#title' => t('Show events for the next # days'),
    '#description' => t('Maximum: 365'),
    '#type' => 'textfield',
    '#maxlength' => 3,
    '#size' => 6,
    '#default_value' => (!empty($conf['localist_settings']['localist_days_ahead']))
                        ? $conf['localist_settings']['localist_days_ahead']
                        : '365',
  );

  // Sort option
  $form['localist_settings']['localist_sort_option'] = array(
    '#title' => t('Sort results by'),
    '#type' => 'select',
    '#options' => array(
                        'date' => 'Event Date',
                        'name' => 'Event Name',
                        'ranking' => 'Event Popularity',
                        'created' => 'Created Date',
                        'updated' => 'Updated Date'
                       ),
    '#default_value' => (!empty($conf['localist_settings']['localist_sort_option']))
                        ? $conf['localist_settings']['localist_sort_option']
                        : 'date',
  );

  /* Localist-provided filters
   * Here, we'll fetch the department list and process them into a user select.
   */
  $filter_departments = array();

  if(!empty($conf['localist_settings']['localist_calendar_url'])) {
    // Fetch list of available filters
    $url = sprintf('%s/api/2/events/filters', $conf['localist_settings']['localist_calendar_url']);
    $options = array();

    $raw_result = drupal_http_request($url, $options);
    $filters = json_decode($raw_result->data);

    // Unpack results into key/value pairs
    $filter_departments = _buildFilterList($filters, 'departments');
  }

  // Department Filter
  $form['localist_settings']['localist_filter_department'] = array(
    '#title' => t('Department'),
    '#type' => 'select',
    '#options' => $filter_departments,
    '#default_value' => (!empty($conf['localist_settings']['localist_filter_department']))
                        ? $conf['localist_settings']['localist_filter_department']
                        : 0,
  );

  return $form;
}

// Save the settings to the $conf array
function wcmc_localist_pane_content_type_edit_form_submit($form, &$form_state) {
  foreach (array_keys($form_state['plugin']['defaults']['localist_settings']) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf']['localist_settings'][$key] = $form_state['values'][$key];
    }
  }
}

/**
 * 'admin info' callback for panel pane.
 * This provides some idea of what the pane will display when viewing in the admin
 */
function wcmc_localist_pane_content_type_admin_info($subtype, $conf, $contexts) {
  if (!empty($conf)) {
    $block = new stdClass;
    $block->title = 'WCMC Localist';
    $block->content = 'This pane pulls in events from the Localist feed.';
    return $block;
  }
}

/**
* Output function for the Custom Pane.
* This does the work of displaying the contents of the custom pane.
* In this example, it passes config to a template using a theme() function.
* The theme function must be registered in my_panes.module.
*/
function wcmc_localist_pane_content_type_render($subtype, $conf, $args, $context = NULL) {
  $block = new stdClass();

  // Prepare request
  $url = sprintf('%s/api/2/events?pp=%s&days=%s&sort=%s',
                  $conf['localist_settings']['localist_calendar_url'],
                  $conf['localist_settings']['localist_display_quantity'],
                  $conf['localist_settings']['localist_days_ahead'],
                  $conf['localist_settings']['localist_sort_option']
                );

    // Append boolean arguments
    if($conf['localist_settings']['localist_filter_department'])
      $url .= '&type[]=' . $conf['localist_settings']['localist_filter_department'];

  $block->content = _wcmc_localist_fetch_events($url, $conf);
  return $block;
}

/**
 * Retrieve profiles from the POPS API.
 *
 * @param $url
 * URL for API Index output
 */
function _wcmc_localist_fetch_events($url, $conf) {
  $http_result = drupal_http_request($url);

  if ($http_result->code == 200) {
    // Prepare JSON content for use
    $data = json_decode($http_result->data);

    $output = '';

    if ($data->events == NULL) {

      $output = 'There are no events scheduled at this time. Check back later.';

    } else {

      // Prepare some vars to pass to display template
      foreach($data->events as $event) {

        // Basic data
        $event_title = $event->event->title;
        $event_localist_url = $event->event->localist_url;
        $event_location = $event->event->location_name;
        $event_location_url = $event->event->venue_url;
        $event_url = $event->event->url;

        // Grab the event image
        $huge_image_url = $event->event->photo_url;

        // Check if it's the placeholder image by noting directory in url
        if (strpos($huge_image_url,'assets') !== false) {
          $event_image =  '';
        } else {
          // for all other event images, get the "big" version instead of "huge" by changing keyword in the url
          $event_image = str_replace('huge', 'big', $huge_image_url);
        }

        // Use divination to tease out the correct date(s)
        foreach($event->event->event_instances as $date) {
          $start = $date->event_instance->start;
          $end = $date->event_instance->end;
          $start_time = strtotime($start);
          $end_time = strtotime($end);
          $all_day = $date->event_instance->all_day;

          $event_date = date('M d, Y', $start_time);

          if ($all_day != TRUE) {
            $event_time = date('g:ia', $start_time) . ' to ' . date('g:ia', $end_time);
          } else {
            $event_time = date('g:ia', $start_time);
          }
        }

        // Grab the description and use a Drupal function to truncate it for a teaser
        // http://api.drupalize.me/api/drupal/function/views_trim_text/7
        $event_full_text = $event->event->description_text;

        //$alter['html'] = TRUE;
        $alter['ellipsis'] = TRUE;
        $alter['word_boundary'] = TRUE;
        $alter['max_length'] = 180;
        $event_description = views_trim_text($alter, $event_full_text);

        // Pass the data through templates/wcmc-localist-pane.tpl.php for rendering
        $event_render = theme('wcmc_localist_pane', array(
          'event_title' => $event_title,
          'event_localist_url' => $event_localist_url,
          'event_location' => $event_location,
          'event_location_url' => $event_location_url,
          'event_url' => $event_url,
          'event_image' => $event_image,
          'event_date' => $event_date,
          'event_time' => $event_time,
          'event_description' => $event_description,
        ));

        $output .= $event_render;
      }
    }

    return $output;

  } else {
    // Log an error if there is a problem
    watchdog('wcmc_localist_pane', 'Unable to retrieve the Localist events. The following HTTP Response Code was received from the Localist API: %code', array('%code' => $http_result->code), WATCHDOG_NOTICE);
    return t('Unable to retrieve the Localist events.');
  }
}

/**
 * Helper function for building lists of filter options
 * @param  object $filters     json-decoded object with filter information
 * @param  string $filter_type Name of the filter to build a list of
 * @return array               Array of available filters to pass to Drupal forms API
 */
function _buildFilterList($filters, $filter_type) {
  $parent_list = array(); // List of top-level items
  $filter_list = array(); // Final list of filters that we'll be returning

  // Get 'parent' items first
  foreach($filters->$filter_type as $filter) {
    if(is_null($filter->parent_id)) {
      $parent_list[$filter->id] = $filter->name;
    }
  }

  // Alphabetically sort list
  asort($parent_list);

  // Iterate over parents and fetch children
  foreach ($parent_list as $parent_id => $parent_name) {
    $children = array();

    foreach ($filters->$filter_type as $filter) {
      if($filter->parent_id == $parent_id) {
        $children[$filter->id] = '- ' . $filter->name;
      }
    }

    asort($children);

    // Add parent with children to final list
    $filter_list = $filter_list + array($parent_id => $parent_name) + $children;

    unset($children);
  }

  // Prepend 'Any' value and return
  return array('0' => 'Any') + $filter_list;
}


<?php
/**
 * @file
 * Display template for the WCMC Localist Pane
 * The template has access to the vars passed from the theme function in wcmc_localist_pane.inc
 */
?>

 <div class="localist-event view-teaser">
  <?php if ($event_image != '') { ?>
  <a href="<?php print $event_localist_url; ?>">
    <img src="<?php print $event_image; ?>" alt="<?php print $event_title; ?>" />
  </a>
  <?php } ?>
  <div class="event-details">
    <h2 class="event-title"><a href="<?php print $event_localist_url; ?>"><?php print $event_title; ?></a></h2>
    <p class="date">
      <span class="day"><?php print $event_date; ?></span><br>
      <span class="time"><?php print $event_time; ?></span>
    </p>
    <p class="venue">
      <a href="<?php print $event_location_url; ?>"><?php print $event_location; ?></a>
    </p>
    <div class="event-description"><?php print $event_description; ?></div>
    <?php if ($event_url != '') { ?>
      <p class="event-website"><a href="<?php print $event_url; ?>">Website</a></p>
    <?php } ?>
  </div>
 </div>
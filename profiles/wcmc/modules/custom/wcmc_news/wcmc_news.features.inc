<?php
/**
 * @file
 * wcmc_news.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function wcmc_news_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "panelizer" && $api == "panelizer") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function wcmc_news_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_node_info().
 */
function wcmc_news_node_info() {
  $items = array(
    'news_post' => array(
      'name' => t('News Post'),
      'base' => 'node_content',
      'description' => t('Post a news article or department update.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}

<?php
/**
 * @file
 * wcmc_news.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wcmc_news_user_default_permissions() {
  $permissions = array();

  // Exported permission: create news_post content.
  $permissions['create news_post content'] = array(
    'name' => 'create news_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any news_post content.
  $permissions['delete any news_post content'] = array(
    'name' => 'delete any news_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own news_post content.
  $permissions['delete own news_post content'] = array(
    'name' => 'delete own news_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any news_post content.
  $permissions['edit any news_post content'] = array(
    'name' => 'edit any news_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own news_post content.
  $permissions['edit own news_post content'] = array(
    'name' => 'edit own news_post content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  return $permissions;
}

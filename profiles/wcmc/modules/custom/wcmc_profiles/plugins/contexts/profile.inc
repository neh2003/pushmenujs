<?php

/**
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("WCMC Profile"),
  'description' => t('Provides display data for Profile panels'),
  'context' => 'wcmc_profile_context_create_profile',  // func to create context
  'context name' => 'profile',
  'keyword' => 'profile',

  // Provides a list of items which are exposed as keywords.
  'convert list' => 'profile_convert_list',
  // Convert keywords into data.
  'convert' => 'profile_convert',
);

/**
 * Create a context
 *
 * @param $empty
 *   If true, just return an empty context.
 * @param $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param $conf
 *   TRUE if the $data is coming from admin configuration, FALSE if it's from a URL arg.
 *
 * @return
 *   a Context object/
 */
function wcmc_profile_context_create_profile($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('profile');
  $context->plugin = 'profile';

  $context->data = new stdClass();
  $context->data->display_name = '';

  // Grab a handle on the node object so we can make some substitutions tokens for use in panelizer
  // http://wearepropeople.com/blog/drupal-breadcrumbs-and-panels-writing-custom-context-plugin

  if (arg(0) == 'node' && is_numeric(arg(1))) {
    $node = node_load(arg(1));

    //Get the values for the name fields and suffixes, so we can mash them up for display.
    // http://www.computerminds.co.uk/articles/rendering-drupal-7-fields-right-way

    $first_name_data = field_get_items('node', $node, 'field_first_name');
    $first_name = render(field_view_value('node', $node, 'field_first_name', $first_name_data[0]));

    $middle_name_data = field_get_items('node', $node, 'field_middle_name');
    if ($middle_name_data != '') {
      $middle_name = render(field_view_value('node', $node, 'field_middle_name', $middle_name_data[0])) . ' ';
    } else {
      $middle_name = '';
    }

    $last_name_data = field_get_items('node', $node, 'field_last_name');
    $last_name = render(field_view_value('node', $node, 'field_last_name', $last_name_data[0]));

    $name_suffix_data = field_get_items('node', $node, 'field_name_suffix');
    if ($name_suffix_data != '') {
      $name_suffix = ', ' . render(field_view_value('node', $node, 'field_name_suffix', $name_suffix_data[0]));
    } else {
      $name_suffix = '';
    }

    $pro_suffix_data = field_get_items('node', $node, 'field_professional_suffix');
    if ($pro_suffix_data != '') {
      $pro_suffix = ', ' . render(field_view_value('node', $node, 'field_professional_suffix', $pro_suffix_data[0]))  ;
    } else {
      $pro_suffix = '';
    }

    $display_name = $first_name . ' ' . $middle_name . $last_name . $name_suffix . $pro_suffix;

    $context->data->display_name = $display_name;

  }

  return $context;
}

/**
 * Provide a list of sub-keywords.
 *
 * This is used to provide keywords from the context for use in a content type,
 * pane, etc.
 */
function profile_convert_list() {
  return array(
    'display_name' => t('Display Name'),
  );
}

/**
 * Convert a context into a string to be used as a keyword by content types, etc.
 */
function profile_convert($context, $type) {
  switch ($type) {
    case 'display_name':
      return $context->data->display_name;
  }
}


<?php
/**
 * @file
 * wcmc_profiles.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function wcmc_profiles_taxonomy_default_vocabularies() {
  return array(
    'person_type' => array(
      'name' => 'Person Type',
      'machine_name' => 'person_type',
      'description' => 'faculty, staff, physician, etc.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
    ),
  );
}

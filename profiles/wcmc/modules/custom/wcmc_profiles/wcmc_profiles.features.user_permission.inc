<?php
/**
 * @file
 * wcmc_profiles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function wcmc_profiles_user_default_permissions() {
  $permissions = array();

  // Exported permission: administer panelizer node profile context.
  $permissions['administer panelizer node profile context'] = array(
    'name' => 'administer panelizer node profile context',
    'roles' => array(),
    'module' => 'panelizer',
  );

  // Exported permission: administer panelizer node profile layout.
  $permissions['administer panelizer node profile layout'] = array(
    'name' => 'administer panelizer node profile layout',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'panelizer',
  );

  // Exported permission: create profile content.
  $permissions['create profile content'] = array(
    'name' => 'create profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any profile content.
  $permissions['delete any profile content'] = array(
    'name' => 'delete any profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: delete own profile content.
  $permissions['delete own profile content'] = array(
    'name' => 'delete own profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any profile content.
  $permissions['edit any profile content'] = array(
    'name' => 'edit any profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  // Exported permission: edit own profile content.
  $permissions['edit own profile content'] = array(
    'name' => 'edit own profile content',
    'roles' => array(
      'administrator' => 'administrator',
      'editor' => 'editor',
      'super user' => 'super user',
    ),
    'module' => 'node',
  );

  return $permissions;
}

<?php
/**
 * @file
 * wcmc_profiles.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function wcmc_profiles_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'profiles';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Profiles';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'perm';
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Field: Content: Profile Image */
  $handler->display->display_options['fields']['field_profile_image']['id'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['table'] = 'field_data_field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['field'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['label'] = '';
  $handler->display->display_options['fields']['field_profile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_profile_image']['settings'] = array(
    'image_style' => 'wcmc_profile_table',
    'image_link' => '',
  );
  /* Field: First Name (not displayed) */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['ui_name'] = 'First Name (not displayed)';
  $handler->display->display_options['fields']['field_first_name']['label'] = '';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_first_name']['element_default_classes'] = FALSE;
  /* Field: Middle Name (not displayed) */
  $handler->display->display_options['fields']['field_middle_name']['id'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['table'] = 'field_data_field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['field'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['ui_name'] = 'Middle Name (not displayed)';
  $handler->display->display_options['fields']['field_middle_name']['label'] = '';
  $handler->display->display_options['fields']['field_middle_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_middle_name']['element_label_colon'] = FALSE;
  /* Field: Last Name (not displayed) */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['ui_name'] = 'Last Name (not displayed)';
  $handler->display->display_options['fields']['field_last_name']['label'] = '';
  $handler->display->display_options['fields']['field_last_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['element_label_colon'] = FALSE;
  /* Field: Name Suffix (not displayed) */
  $handler->display->display_options['fields']['field_name_suffix']['id'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['table'] = 'field_data_field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['field'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['ui_name'] = 'Name Suffix (not displayed)';
  $handler->display->display_options['fields']['field_name_suffix']['label'] = '';
  $handler->display->display_options['fields']['field_name_suffix']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['text'] = ' [field_name_suffix], ';
  $handler->display->display_options['fields']['field_name_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_name_suffix']['hide_empty'] = TRUE;
  /* Field: Display Name (combined fields) */
  $handler->display->display_options['fields']['field_professional_suffix']['id'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['table'] = 'field_data_field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['field'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['ui_name'] = 'Display Name (combined fields)';
  $handler->display->display_options['fields']['field_professional_suffix']['label'] = 'Faculty';
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['text'] = '<h2 class="display-name">[field_first_name] [field_middle_name] [field_last_name], [field_name_suffix][field_professional_suffix]</h2> ';
  $handler->display->display_options['fields']['field_professional_suffix']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_professional_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_suffix']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_professional_suffix']['element_default_classes'] = FALSE;
  /* Field: Content: Professional Title */
  $handler->display->display_options['fields']['field_professional_title']['id'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['table'] = 'field_data_field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['field'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['label'] = 'Title';
  $handler->display->display_options['fields']['field_professional_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_title']['delta_offset'] = '0';
  /* Field: Content: Phone */
  $handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
  $handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Full profile link */
  $handler->display->display_options['fields']['field_pops_profile_link']['id'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['table'] = 'field_data_field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['field'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['label'] = 'POPS Profile';
  $handler->display->display_options['fields']['field_pops_profile_link']['click_sort_column'] = 'url';
  /* Sort criterion: Content: Is Business Unit Leader (field_is_business_unit_leader) */
  $handler->display->display_options['sorts']['field_is_business_unit_leader_value']['id'] = 'field_is_business_unit_leader_value';
  $handler->display->display_options['sorts']['field_is_business_unit_leader_value']['table'] = 'field_data_field_is_business_unit_leader';
  $handler->display->display_options['sorts']['field_is_business_unit_leader_value']['field'] = 'field_is_business_unit_leader_value';
  $handler->display->display_options['sorts']['field_is_business_unit_leader_value']['order'] = 'DESC';
  /* Sort criterion: Content: Last Name (field_last_name) */
  $handler->display->display_options['sorts']['field_last_name_value']['id'] = 'field_last_name_value';
  $handler->display->display_options['sorts']['field_last_name_value']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['sorts']['field_last_name_value']['field'] = 'field_last_name_value';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
  );
  /* Filter criterion: Content: Last Name (field_last_name) */
  $handler->display->display_options['filters']['field_last_name_value']['id'] = 'field_last_name_value';
  $handler->display->display_options['filters']['field_last_name_value']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['filters']['field_last_name_value']['field'] = 'field_last_name_value';
  $handler->display->display_options['filters']['field_last_name_value']['operator'] = 'contains';
  $handler->display->display_options['filters']['field_last_name_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_last_name_value']['expose']['operator_id'] = 'field_last_name_value_op';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['label'] = 'Last Name';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['operator'] = 'field_last_name_value_op';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['identifier'] = 'field_last_name_value';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
  );
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_filter'] = 1;
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_items'] = '10';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_field'] = 'field_last_name';
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_raw_suggestion'] = 1;
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_raw_dropdown'] = 1;
  $handler->display->display_options['filters']['field_last_name_value']['expose']['autocomplete_dependent'] = 0;

  /* Display: Profile Directory Table */
  $handler = $view->new_display('panel_pane', 'Profile Directory Table', 'panel_pane_1');
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Search';
  $handler->display->display_options['defaults']['pager'] = FALSE;
  $handler->display->display_options['pager']['type'] = 'none';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Content: Profile Image */
  $handler->display->display_options['fields']['field_profile_image']['id'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['table'] = 'field_data_field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['field'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['label'] = '';
  $handler->display->display_options['fields']['field_profile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_profile_image']['settings'] = array(
    'image_style' => 'wcmc_profile_table',
    'image_link' => '',
  );
  /* Field: First Name (not displayed) */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['ui_name'] = 'First Name (not displayed)';
  $handler->display->display_options['fields']['field_first_name']['label'] = '';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_first_name']['element_default_classes'] = FALSE;
  /* Field: Middle Name (not displayed) */
  $handler->display->display_options['fields']['field_middle_name']['id'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['table'] = 'field_data_field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['field'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['ui_name'] = 'Middle Name (not displayed)';
  $handler->display->display_options['fields']['field_middle_name']['label'] = '';
  $handler->display->display_options['fields']['field_middle_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_middle_name']['element_label_colon'] = FALSE;
  /* Field: Last Name (not displayed) */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['ui_name'] = 'Last Name (not displayed)';
  $handler->display->display_options['fields']['field_last_name']['label'] = '';
  $handler->display->display_options['fields']['field_last_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['element_label_colon'] = FALSE;
  /* Field: Name Suffix (not displayed) */
  $handler->display->display_options['fields']['field_name_suffix']['id'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['table'] = 'field_data_field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['field'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['ui_name'] = 'Name Suffix (not displayed)';
  $handler->display->display_options['fields']['field_name_suffix']['label'] = '';
  $handler->display->display_options['fields']['field_name_suffix']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['text'] = ' [field_name_suffix], ';
  $handler->display->display_options['fields']['field_name_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_name_suffix']['hide_empty'] = TRUE;
  /* Field: Display Name (combined fields) */
  $handler->display->display_options['fields']['field_professional_suffix']['id'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['table'] = 'field_data_field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['field'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['ui_name'] = 'Display Name (combined fields)';
  $handler->display->display_options['fields']['field_professional_suffix']['label'] = 'Faculty';
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['text'] = '<h4 class="display-name">[field_first_name] [field_middle_name] [field_last_name], [field_name_suffix][field_professional_suffix]</h4> ';
  $handler->display->display_options['fields']['field_professional_suffix']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_professional_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_suffix']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_professional_suffix']['element_default_classes'] = FALSE;
  /* Field: Content: Professional Title */
  $handler->display->display_options['fields']['field_professional_title']['id'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['table'] = 'field_data_field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['field'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['label'] = 'Title';
  $handler->display->display_options['fields']['field_professional_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_title']['delta_offset'] = '0';
  /* Field: Content: Phone */
  $handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
  $handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['element_label_colon'] = FALSE;
  /* Field: Content: Full profile link */
  $handler->display->display_options['fields']['field_pops_profile_link']['id'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['table'] = 'field_data_field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['field'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['label'] = '';
  $handler->display->display_options['fields']['field_pops_profile_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pops_profile_link']['click_sort_column'] = 'url';
  /* Field: Content: Person Type */
  $handler->display->display_options['fields']['field_person_type_taxonomy']['id'] = 'field_person_type_taxonomy';
  $handler->display->display_options['fields']['field_person_type_taxonomy']['table'] = 'field_data_field_person_type_taxonomy';
  $handler->display->display_options['fields']['field_person_type_taxonomy']['field'] = 'field_person_type_taxonomy';
  $handler->display->display_options['fields']['field_person_type_taxonomy']['delta_offset'] = '0';
  $handler->display->display_options['pane_category']['name'] = 'Profiles';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $handler->display->display_options['inherit_panels_path'] = '1';

  /* Display: Profile List View */
  $handler = $view->new_display('panel_pane', 'Profile List View', 'panel_pane_2');
  $handler->display->display_options['defaults']['style_plugin'] = FALSE;
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['style_options']['default_row_class'] = FALSE;
  $handler->display->display_options['defaults']['style_options'] = FALSE;
  $handler->display->display_options['defaults']['row_plugin'] = FALSE;
  $handler->display->display_options['row_plugin'] = 'fields';
  $handler->display->display_options['defaults']['row_options'] = FALSE;
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Text area */
  $handler->display->display_options['empty']['area']['id'] = 'area';
  $handler->display->display_options['empty']['area']['table'] = 'views';
  $handler->display->display_options['empty']['area']['field'] = 'area';
  $handler->display->display_options['empty']['area']['empty'] = TRUE;
  $handler->display->display_options['empty']['area']['content'] = 'There are no profiles that match that search.';
  $handler->display->display_options['empty']['area']['format'] = 'panopoly_wysiwyg_text';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Link to POPS (not displayed) */
  $handler->display->display_options['fields']['field_pops_profile_link_1']['id'] = 'field_pops_profile_link_1';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['table'] = 'field_data_field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['field'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['ui_name'] = 'Link to POPS (not displayed)';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['label'] = '';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_pops_profile_link_1']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pops_profile_link_1']['click_sort_column'] = 'url';
  $handler->display->display_options['fields']['field_pops_profile_link_1']['type'] = 'link_plain';
  /* Field: Content: Profile Image */
  $handler->display->display_options['fields']['field_profile_image']['id'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['table'] = 'field_data_field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['field'] = 'field_profile_image';
  $handler->display->display_options['fields']['field_profile_image']['label'] = '';
  $handler->display->display_options['fields']['field_profile_image']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_profile_image']['alter']['text'] = '<a target="_blank" href="[field_pops_profile_link_1] ">[field_profile_image]</a>';
  $handler->display->display_options['fields']['field_profile_image']['element_type'] = 'div';
  $handler->display->display_options['fields']['field_profile_image']['element_class'] = 'profile-list-image';
  $handler->display->display_options['fields']['field_profile_image']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_profile_image']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_profile_image']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_profile_image']['click_sort_column'] = 'fid';
  $handler->display->display_options['fields']['field_profile_image']['settings'] = array(
    'image_style' => '',
    'image_link' => '',
  );
  /* Field: First Name (not displayed) */
  $handler->display->display_options['fields']['field_first_name']['id'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['table'] = 'field_data_field_first_name';
  $handler->display->display_options['fields']['field_first_name']['field'] = 'field_first_name';
  $handler->display->display_options['fields']['field_first_name']['ui_name'] = 'First Name (not displayed)';
  $handler->display->display_options['fields']['field_first_name']['label'] = '';
  $handler->display->display_options['fields']['field_first_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_first_name']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_first_name']['element_default_classes'] = FALSE;
  /* Field: Middle Name (not displayed) */
  $handler->display->display_options['fields']['field_middle_name']['id'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['table'] = 'field_data_field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['field'] = 'field_middle_name';
  $handler->display->display_options['fields']['field_middle_name']['ui_name'] = 'Middle Name (not displayed)';
  $handler->display->display_options['fields']['field_middle_name']['label'] = '';
  $handler->display->display_options['fields']['field_middle_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_middle_name']['element_label_colon'] = FALSE;
  /* Field: Last Name (not displayed) */
  $handler->display->display_options['fields']['field_last_name']['id'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['fields']['field_last_name']['field'] = 'field_last_name';
  $handler->display->display_options['fields']['field_last_name']['ui_name'] = 'Last Name (not displayed)';
  $handler->display->display_options['fields']['field_last_name']['label'] = '';
  $handler->display->display_options['fields']['field_last_name']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_last_name']['element_label_colon'] = FALSE;
  /* Field: Name Suffix (not displayed) */
  $handler->display->display_options['fields']['field_name_suffix']['id'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['table'] = 'field_data_field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['field'] = 'field_name_suffix';
  $handler->display->display_options['fields']['field_name_suffix']['ui_name'] = 'Name Suffix (not displayed)';
  $handler->display->display_options['fields']['field_name_suffix']['label'] = '';
  $handler->display->display_options['fields']['field_name_suffix']['exclude'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_name_suffix']['alter']['text'] = ' [field_name_suffix], ';
  $handler->display->display_options['fields']['field_name_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_name_suffix']['hide_empty'] = TRUE;
  /* Field: Display Name (combined fields) */
  $handler->display->display_options['fields']['field_professional_suffix']['id'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['table'] = 'field_data_field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['field'] = 'field_professional_suffix';
  $handler->display->display_options['fields']['field_professional_suffix']['ui_name'] = 'Display Name (combined fields)';
  $handler->display->display_options['fields']['field_professional_suffix']['label'] = '';
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_professional_suffix']['alter']['text'] = '<h2 class="display-name">[field_first_name] [field_middle_name] [field_last_name], [field_name_suffix][field_professional_suffix]</h2> ';
  $handler->display->display_options['fields']['field_professional_suffix']['element_type'] = 'h2';
  $handler->display->display_options['fields']['field_professional_suffix']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_suffix']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_professional_suffix']['element_default_classes'] = FALSE;
  /* Field: Content: Professional Title */
  $handler->display->display_options['fields']['field_professional_title']['id'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['table'] = 'field_data_field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['field'] = 'field_professional_title';
  $handler->display->display_options['fields']['field_professional_title']['label'] = '';
  $handler->display->display_options['fields']['field_professional_title']['element_type'] = 'h3';
  $handler->display->display_options['fields']['field_professional_title']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_professional_title']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_professional_title']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_professional_title']['delta_offset'] = '0';
  /* Field: Content: Phone */
  $handler->display->display_options['fields']['field_phone']['id'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['table'] = 'field_data_field_phone';
  $handler->display->display_options['fields']['field_phone']['field'] = 'field_phone';
  $handler->display->display_options['fields']['field_phone']['label'] = '';
  $handler->display->display_options['fields']['field_phone']['alter']['alter_text'] = TRUE;
  $handler->display->display_options['fields']['field_phone']['alter']['text'] = '<p class="profile-phone>Phone: [field_phone]</p>';
  $handler->display->display_options['fields']['field_phone']['element_type'] = '0';
  $handler->display->display_options['fields']['field_phone']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_phone']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_phone']['element_default_classes'] = FALSE;
  /* Field: Content: Full profile link */
  $handler->display->display_options['fields']['field_pops_profile_link']['id'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['table'] = 'field_data_field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['field'] = 'field_pops_profile_link';
  $handler->display->display_options['fields']['field_pops_profile_link']['label'] = '';
  $handler->display->display_options['fields']['field_pops_profile_link']['element_type'] = 'p';
  $handler->display->display_options['fields']['field_pops_profile_link']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_pops_profile_link']['element_wrapper_type'] = '0';
  $handler->display->display_options['fields']['field_pops_profile_link']['element_default_classes'] = FALSE;
  $handler->display->display_options['fields']['field_pops_profile_link']['click_sort_column'] = 'url';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Content: Last Name (field_last_name) */
  $handler->display->display_options['arguments']['field_last_name_value']['id'] = 'field_last_name_value';
  $handler->display->display_options['arguments']['field_last_name_value']['table'] = 'field_data_field_last_name';
  $handler->display->display_options['arguments']['field_last_name_value']['field'] = 'field_last_name_value';
  $handler->display->display_options['arguments']['field_last_name_value']['default_argument_type'] = 'fixed';
  $handler->display->display_options['arguments']['field_last_name_value']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['field_last_name_value']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['field_last_name_value']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['arguments']['field_last_name_value']['glossary'] = TRUE;
  $handler->display->display_options['arguments']['field_last_name_value']['limit'] = '1';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 1;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'profile' => 'profile',
  );
  $handler->display->display_options['pane_category']['name'] = 'Profiles';
  $handler->display->display_options['pane_category']['weight'] = '0';
  $export['profiles'] = $view;

  return $export;
}

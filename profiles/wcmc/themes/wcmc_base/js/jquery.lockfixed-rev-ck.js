/*!
 * jQuery lockfixed plugin
 * http://www.directlyrics.com/code/lockfixed/
 *
 * Copyright 2012 Yvo Schaap
 * Released under the MIT license
 * http://www.directlyrics.com/code/lockfixed/license.txt
 *
 * Date: Sun Jan 27 2013 12:00:00 GMT
 *
 * This script has been revised to add an if statement that checks a css value only available when in desktop size (line 50).
 * It also calculates the bottom offset from the height of the page footer. (line 62)
 */(function(e,t){e.extend({lockfixed:function(t,n){n&&n.offset?(n.offset.bottom=parseInt(n.offset.bottom,10),n.offset.top=parseInt(n.offset.top,10)):n.offset={bottom:305,top:0};var t=e(t);if(t&&t.offset()){var r=t.offset().top,i=t.offset().left,s=t.outerHeight(!0),o=t.outerWidth(),u=t.css("position"),a=t.css("top"),f=0,l=e(document).height()-n.offset.bottom,c=0,h=!1,p=!1;if(n.forcemargin===!0||navigator.userAgent.match(/\bMSIE (4|5|6)\./)||navigator.userAgent.match(/\bOS (3|4|5|6)_/)||navigator.userAgent.match(/\bAndroid (1|2|3|4)\./i))p=!0;e(window).bind("scroll resize orientationchange load",t,function(i){if(e(".information-column").css("float")==="left"&&e(".information-column").height()<e("#main-content").height()){var s=t.outerHeight(),h=e(window).scrollTop();r=t.offset().top;if(p&&document.activeElement&&document.activeElement.nodeName==="INPUT")return;var d=t.parent().offset().top;h>=d-(f?f:0)-n.offset.top?(e(".logged-in").length?n.offset.bottom=e("footer").height()+80:n.offset.bottom=e("footer").height()+20,l=e(document).height()-n.offset.bottom,l<h+s+f+n.offset.top?c=h+s+f+n.offset.top-l:c=0,p?t.css({marginTop:parseInt((f?f:0)+(h-r-c)+2*n.offset.top,10)+"px"}):t.css({position:"fixed",top:n.offset.top-c+"px",width:o+"px"})):t.css({position:u,top:a,width:o+"px",marginTop:(f?f:0)+"px"})}else t.css({position:"static"})})}}})})(jQuery);
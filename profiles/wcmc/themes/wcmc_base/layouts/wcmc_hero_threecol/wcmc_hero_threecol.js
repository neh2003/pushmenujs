(function($){
  $(document).ready(function() {
    //Duplicates the related-content sidebar contents into an area below the fold for smaller screens
    $("#related-content-sidebar").find(".panel-pane").clone().appendTo("#related-content-inline");
  });
})(jQuery);
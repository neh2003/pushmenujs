<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('WCMC Two Column Right'),
  'category' => t('WCMC'),
  'icon' => 'wcmc_twocol.png',
  'theme' => 'wcmc_twocol',
  'theme arguments' => array('id', 'content'),
  'css' => 'wcmc_twocol.css',
  'regions' => array(
    'main_content' => t('Main Content'),
    'information_sidebar' => t('Information Sidebar'),
  ),
);


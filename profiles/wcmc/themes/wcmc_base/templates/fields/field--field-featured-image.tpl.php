<?php if (!$label_hidden) : ?>
  <h2 class="field-label"<?php print $title_attributes; ?>><?php print $label ?>:&nbsp;</h2>
<?php endif; ?>
<?php foreach ($items as $delta => $item): ?>
  <figure class="hero-image">
    <?php
      // Assign a new var so we can manipulate before rendering
      $featured_image = $item;
      // Strip out any HTML so that the default browser display of the title attribute looks better
      $featured_image['#item']['title'] = strip_tags($item['#item']['title']);
    ?>
    <?php print render($featured_image); ?>
    <?php if ($item['#item']['title']): ?>
      <figcaption><?php print $item['#item']['title']; ?></figcaption>
    <?php endif; ?>
  </figure>

<?php endforeach; ?>
<div id="page">
  <header role="banner">
    <div class="wrap">
      <div class="dual-branding">
        <a class="wcmc-emblem" href="http://weill.cornell.edu">Weill Cornell Medical College</a>
        <a class="nyp-brand" href="http://nyp.org">NewYork-Presbyterian Hospital</a>
      </div>
      <?php print $wcmc_search_widget; ?>
      <div class="site-branding">
        <h1><a class="go-home" href="/"><?php print $site_name; ?><span class="affiliation">at Weill Cornell Medical College</span></a></h1>
        <div class="menu-button ss-rows"></div>
      </div>
    </div>
  </header>

  <nav id="top-nav" role="navigation">
    <?php print $wcmc_search_mobile_widget; ?>
    <?php if (isset($primary_nav)) { print $primary_nav; } ?>
    <?php if (isset($drawer_nav)) { print $drawer_nav; } ?>
    <?php if (isset($active_second_level_nav)) { print $active_second_level_nav; } ?>
    <?php if (isset($active_third_level_nav)) { print $active_third_level_nav; } ?>
  </nav><!-- /#navigation -->

  <div class="body-wrap">
    <div id="main">
      <?php print $messages; ?>
      <?php print $breadcrumb; ?>
      <h1 class="title"><?php print $title; ?></h1>
      <?php if ($tabs = render($tabs)): ?>
        <div id="tabs">
          <?php print $tabs; ?>
        </div>
      <?php endif; ?>
      <?php if (isset($mobile_sub_nav)) { ?>
        <nav id="body-nav" role="navigation">
          <?php print $mobile_sub_nav; ?>
        </nav>
      <?php } ?>
      <?php print render($page['content']); ?>
      <?php if (isset($second_level_nav) && $level == 1) { ?>
        <nav id="second-level-nav" role="navigation">
          <?php print $second_level_nav; ?>
        </nav>
      <?php } ?>
    </div><!--/#main-->
  </div>

  <footer role="contentinfo">
    <div class="footer-site">
      <nav class="footer-primary-nav">
        <?php if (isset($footer_nav)) { print $footer_nav; } ?>
      </nav>
      <div class="department">
        <div class="site-name"><!-- Department of<wbr> --> <?php print $site_name; ?></div>
        <div class="location">
          <a class="icon-button ss-icon map-link" href="<?php print $site_map_link;?>">Location</a>
          <div class="affiliation"><?php print $site_affiliation; ?></div>
          <div class="address"><?php print $site_street_address; ?> New York, NY <?php print $site_zip; ?></div>
        </div>
      </div>
      <div class="contact">
        <div class="phone">
          Phone: <?php print $site_phone; ?>
          <?php if (!empty($site_fax)) { ?>
            | Fax: <?php print $site_fax; ?>
          <?php } ?>
        </div>
        <ul class="social-links">
          <?php if (!empty($social_facebook)) { ?>
          <li class="facebook-icon-container"><span class="facebook-button-background"></span><a class="wcmc-social-button-facebook ss-social-regular ss-icon" href="<?php print $social_facebook;?>">Facebook</a></li>
          <?php } ?>
          <?php if (!empty($social_twitter)) { ?>
          <li><a class="wcmc-social-button ss-social-regular ss-icon" href="<?php print $social_twitter;?>">Twitter</a></li>
          <?php } ?>
          <?php if (!empty($social_youtube)) { ?>
          <li><a class="wcmc-social-button ss-social-regular ss-icon" href="<?php print $social_youtube;?>">Youtube</a></li>
          <?php } ?>
          <?php if (!empty($site_mail)) { ?>
          <li><a class="ss-icon wcmc-social-button" href="mailto:<?php print $site_mail; ?>">Email</a></li>
          <?php } ?>
        </ul>
      </div>
      <div class="footer-wcmc">
        <div class="wcmc-logo">Weill Cornell Medical College</div>
        <div class="nyp-logo">NewYork-Presbyterian</div>
        <div class="disclaimer">
         Unless otherwise noted, © Weill Cornell Medical College. <a href="http://weill.cornell.edu/legal/">Legal information</a>
        </div>
        <?php print render($page['footer']); ?>
      </div>
    </div>
  </footer>
</div>

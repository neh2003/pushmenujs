<?php


/**
 * Implements hook_install_tasks()
 */
function wcmc_install_tasks(&$install_state) {

  // Add our custom CSS file for the installation process
  drupal_add_css(drupal_get_path('profile', 'wcmc') . '/wcmc.css');
  drupal_set_title('Weill Cornell Medical College | Site Installation');

  // Add a custom config form to the installation steps, uses wcmc_settings()
  $tasks = array(
    'wcmc_settings' => array(
      'display_name' => st('WCMC Configuration'),
      'type' => 'form',
    ),
  );
  return $tasks;
}

/**
 * Implements hook_install_tasks_alter()
 */
function wcmc_install_tasks_alter(&$tasks, $install_state) {

  // Magically go one level deeper in solving years of dependency problems
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_load_profile']['function'] = 'panopoly_core_install_load_profile';

  // Since we only offer one language, define a callback to set this
  require_once(drupal_get_path('module', 'panopoly_core') . '/panopoly_core.profile.inc');
  $tasks['install_select_locale']['function'] = 'panopoly_core_install_locale_selection';
}

/**
 * Implements hook_form_FORM_ID_alter()
 */
function wcmc_form_install_configure_form_alter(&$form, $form_state) {

  // Hide some messages from various modules that are just too chatty.
  drupal_get_messages('status');
  drupal_get_messages('warning');

  // Set reasonable defaults for site configuration form
  $form['site_information']['site_name']['#default_value'] = 'Department Name';
  $form['admin_account']['account']['name']['#default_value'] = 'admin';
  $form['server_settings']['site_default_country']['#default_value'] = 'US';
  $form['server_settings']['date_default_timezone']['#default_value'] = 'America/New_York';

  // Define a default email address pattern
  $form['site_information']['site_mail']['#default_value'] = 'cwid@med.cornell.edu';
  $form['admin_account']['account']['mail']['#default_value'] = 'cwid@med.cornell.edu';
  $form['site_information']['site_mail']['#description'] = 'Enter a contact email address for this site.';
}

/**
 * Builds and returns the settings form for the WCMC configuration step
 */
function wcmc_settings() {

  // Collect Contact Information
  $form['contact_info'] = array(
    '#type' => 'fieldset',
    '#title' => t('Contact Information'),
  );

  // Adjust site information fieldset to capture contact info
  $form['contact_info']['site_affiliation'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Affiliation'),
    '#default_value' => 'Weill Cornell Medical College',
    '#description'   => t('Enter the institutional affiliation.'),
  );

  $form['contact_info']['site_street_address'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Street address'),
    '#default_value' => '575 Lexington Avenue, 5 FL',
    '#description'   => t('Enter a street address'),
  );

  $form['contact_info']['site_phone'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Phone'),
    '#default_value' => '(212) 746-5555',
    '#description'   => t('Enter a contact phone number'),
  );

  $form['contact_info']['site_zip'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Zip Code'),
    '#default_value' => '10022',
    '#size' => 5,
    '#maxlength' => 5,
    '#description'   => t('Enter a five-digit zip code.'),
  );

  // Collect SMTP Email Settings
  $form['auth'] = array(
    '#type'        => 'fieldset',
    '#title'       => t('Email Authentication'),
    '#description' => t('Provide CWID for mail server authentication.'),
  );
  $form['auth']['smtp_username'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Username'),
    '#description'   => t('CWID'),
  );
  $form['auth']['smtp_password'] = array(
    '#type'          => 'password',
    '#title'         => t('Password'),
    '#description'   => t('Account password'),
  );

  $form['email_options'] = array(
    '#type'  => 'fieldset',
    '#title' => t('Email options'),
  );
  $form['email_options']['smtp_from'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email from address'),
    '#default_value' => 'do-not-reply@med.cornell.edu',
    '#description'   => t('The email address that all emails will be from.'),
  );
  $form['email_options']['smtp_fromname'] = array(
    '#type'          => 'textfield',
    '#title'         => t('Email from name'),
    '#default_value' => 'Weill Cornell Medical College',
    '#description'   => t('The name that all e-mails will be from.'),
  );

  return system_settings_form($form);
}

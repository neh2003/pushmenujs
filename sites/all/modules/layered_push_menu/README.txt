This Drupal module implements a Jquery Multi Level Push menu. More information on the JQuery plugin used can be found here..
http://multi-level-push-menu.make.rs/

In order to work this module requires certain markup in the page.tpl.php file of the theme being used

<div id="menu">
  <nav>
    
      <?php if ($page['menuright']): ?>
        <?php print render($page['menuright']); ?>
      <?php endif; ?>
              
    </nav>
</div>
<div id = "page">
	<!-- Your page.tpl.php regular code here-->
</div>

A new template file will also need to be included called region--menuright.tpl.php in the templates folder of you current theme. It should contain the following code...

<?php if ($content): ?>
  
    <?php print $content; ?>
  
<?php endif; ?>

drupal backend, the Push Navigation block should be placed in menuright region
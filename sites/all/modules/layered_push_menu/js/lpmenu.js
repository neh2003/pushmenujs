jQuery( document ).ready(function() {


	/*Function to open menu on current level*/
	function gotolevel(){
		var activeObj = jQuery('.active').parent().siblings("h2").children("a").html();
		jQuery( '#menu' ).multilevelpushmenu( 'expand' , activeObj );
		//jQuery('#page').css('right','300px');
	}
	
	/*Iniate menu as open on current level if window width above 1680*/
	if(window.innerWidth>1680){
		jQuery( '#menu' ).multilevelpushmenu({
			containersToPush: [jQuery( '#page' )],
			collapsed: false,
			menuHeight: jQuery( '#page' ).height(),
			menuWidth: 300,
			direction: 'rtl'
		});	
		jQuery('#page').css('right','300px');
		jQuery('#menu').css('min-width','300px');
		jQuery('#menu_multilevelpushmenu').css('min-width','300px');
		gotolevel();
	}
	/*Iniate menu as closed if window width less than 1680*/
	else{
		jQuery( '#menu' ).multilevelpushmenu({
			containersToPush: [jQuery( '#page' )],
			collapsed: true,
			menuHeight: jQuery( '#page' ).height(),
			menuWidth: 300,
			direction: 'rtl'
		});	
		
	}

	
	/*Workaround for links not firing*/
	jQuery('ul.thistree>li>a:not(.expanded>a)').click(function(){
		window.location.replace(this.href);
	});

	/*Make open/close responsive using window.resize*/
	jQuery(window).resize(function(){
		if (jQuery(window).width() > 1680){	
			jQuery('#menu').multilevelpushmenu( 'redraw' );
			gotolevel();
			//jQuery("#Page").css("right","300px")
			//expand
		}
		else{
			//collapse
			jQuery('#menu').multilevelpushmenu('collapse');
		}
	});

	/*Setting menu height to the height of the page div*/
	var menuh = jQuery( '#page' ).css('height');
	jQuery('#menu').css({
		'height': menuh,
		'min-height': menuh
	});
	if(jQuery("h1.page-title").text() == "Administration"){
		jQuery("#page").css("right","0");
	}

	// 

	// .levelHolderClass .multilevelpushmenu_inactive
	// 	.levelHolderClass

	jQuery('.multilevelpushmenu_inactive').click(function(){
		alert('multilevelpushmenu_inactive');
	});

	// alert('Start');
    // new mlPushMenu( document.getElementById( 'mp-menu' ), document.getElementById( 'trigger' ) );
    // alert('End?');
	
});





jQuery( document ).ready(function() {
    jQuery('#edit-submit--4').insertAfter('#wcmc-search-widget--4 .form-item-search-keys');
	jQuery('#edit-submit.search-button').insertAfter('#wcmc-search-widget .form-item-search-keys');
	jQuery('#edit-submit--2.search-button').insertAfter('#wcmc-search-widget .form-item-search-keys');
	jQuery('#menu-header-content').append('<div id="reset-link"><img src="/sites/default/files/refresh.png" alt="Refresh" width="15"/><p id="reset">Reset This Menu</p></div><div id="close-menu">X CLOSE</div><div class="clear"></div>');
	jQuery('#block-menu-menu-quicklinks').appendTo('.mm-footer');
	
	jQuery('#hamburger').click(function() {
		youAreHere();
	});
	
	jQuery('#reset-link').click(function() {
		menuReset();
		youAreHere();
	});
	
	jQuery('#close-menu').click(function() {
		jQuery('#mmenu_right').trigger("close.mm");
	});
	
	jQuery('#mmenu_right').on('closed.mm', function() {
		  menuReset();
	});
});

function menuReset() {
	var mmenu = jQuery('#mmenu_right');
		  if (mmenu.hasClass('mm-horizontal')) {
			// For horizontal mmenu when slidingSubmenus option is set to Yes.
			jQuery('> .mm-panel', mmenu).addClass('mm-hidden').removeClass('mm-opened mm-subopened mm-highest mm-current');
			jQuery('#mm-0').removeClass('mm-hidden mm-subopened').addClass('mm-opened mm-current');
		  }
		  else {
			// For vertical mmenu when slidingSubmenus option is set to No.
			jQuery('> .mm-panel .mm-opened', mmenu).removeClass('mm-opened');
		  }
}

function youAreHere() {
	jQuery('.mmenu-mm-list-level-2 li').each(function(){
			if(jQuery(this).hasClass('active-trail')) {
				jQuery(this).parents().addClass('mm-opened');
			} 
		});
}


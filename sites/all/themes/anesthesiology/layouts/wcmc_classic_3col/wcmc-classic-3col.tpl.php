<?php
/**
 * @file
 * Template for the 3 column panel layout with hero image.
 *
 *
 * Variables:
 * - $id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 *   panel of the layout. This layout supports the following sections:
 *   - $content['left']: Content in the left column.
 *   - $content['middle']: Content in the middle column.
 *   - $content['right']: Content in the right column.
 */
?>
<div class="panel-classic-3col" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>
  <?php if (!empty($content['header'])) { ?>
    <div class="classic-3col-header">
      <?php print $content['header']; ?>
    </div>
  <?php } ?>
  <div id="main-content" class="main-content" role="main">
    <div id="classic-3col-left">
      <?php print $content['left']; ?>
    </div>
    <div id="classic-3col-center">
      <?php print $content['center']; ?>
    </div>
    <div id="classic-3col-right">
      <?php print $content['right']; ?>
    </div>
  </div>
</div>

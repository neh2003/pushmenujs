<?php

/**
 * Change default separator in the breadcrumb
 */
function anesthesiology_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode('&nbsp; / &nbsp;', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Perform alterations to the JavaScript before it is presented on the page.
 */
function anesthesiology_js_alter(&$javascript) {
  // Typekit - Remove the Base Theme font kit
  unset($javascript['//use.typekit.net/icu8dxt.js']);
}

/**
 * Override or insert variables into the html templates.
 */
function anesthesiology_preprocess_html(&$variables, $hook) {
  // Typekit - Add sub-theme font kit
  drupal_add_js('//use.typekit.net/aqc5rek.js', 'external');
}

/**
 * Override or insert variables into the page template.
 */
function anesthesiology_preprocess_page(&$variables, $hook) {
  if ($variables['is_front']) {
    drupal_add_js(drupal_get_path('theme','anesthesiology') . '/js/jquery.customSelect.min.js');
    drupal_add_js('
    jQuery(document).ready(function() {
      jQuery("#fellowships").customSelect().show();
      jQuery("#fellowships").change(function(){
         window.location.href = this.value;
      })
    });
    ',
    array('type' => 'inline')
    );
  }
}


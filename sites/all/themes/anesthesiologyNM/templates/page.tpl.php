<div id="menu">
  <nav>
    
      <?php if ($page['menuleft']): ?>
        <?php print render($page['menuleft']); ?>
      <?php endif; ?>
              
    </nav>
</div> 

  <div id='page'>
  <header role="banner">
    <div class="wrap">
      <div class="wcmc-branding">
        <a class="wcmc-emblem">Weill Cornell Medical College</a>
        <div class="wcmc-etc">
          <nav class="wcmc-links">
            <a href="http://weill.cornell.edu" class="wcmc-primary-nav-item" target="_blank">WCMC Home</a>
            <a href="http://weill.cornell.edu/education"class="wcmc-primary-nav-item" target="_blank">Medical Education</a>
            <div class="float-block">
              <a href="http://weill.cornell.edu/research" class="wcmc-primary-nav-item" target="_blank">Research</a>
              <a href="http://weillcornell.org" class="wcmc-primary-nav-item" target="_blank">Patient Care</a>
            </div>
          </nav>
          <div class="wcmc-links-expander"></div>
          <div class="global-functions-container">
            <?php if ($site_connect == 1) { ?>
            <a class="weill-cornell-connect" href="http://weillcornell.org/connect" target="_blank">Weill Cornell Connect</a>
            <?php } ?>
            
          </div>
        </div>
      </div>
      <div class="site-branding">
        <h1><a class="go-home" href="/">Anesthesiology<span class="affiliation">at Weill Cornell Medical College</span></a></h1>
        <div class="menu-button ss-rows"></div>
      </div>
    </div>
   
  </header>
  
  <nav id="top-nav" role="navigation">
   <div class="nav-block-inner">
       <div id="search-block"><p class="search-label">Search </p><?php print $wcmc_search_widget; ?></div>
       <!-- <div id="slide-trigger"><a id="hamburger" class="mm-fixed-top" href="#menu" style="-webkit-transform: none;"><span>☰ Open Menu Navigation</span></a></div> -->
       <div class="clear"></div>
   </div>
  </nav><!-- /#navigation -->



  <div class="body-wrap">
    <div id="main">
      <?php print $messages; ?>
      <?php print $breadcrumb; ?>
      <h1 class="title"><?php print $title; ?></h1>
      <?php if ($tabs = render($tabs)): ?>
        <div id="tabs">
          <?php print $tabs; ?>
        </div>
      <?php endif; ?>
      <?php if (isset($mobile_sub_nav)) { ?>
        <nav id="body-nav" role="navigation">
          <?php print $mobile_sub_nav; ?>
        </nav>
      <?php } ?>
      <?php print render($page['content']); ?>
      <?php if (isset($second_level_nav) && $level == 1) { ?>
        <nav id="second-level-nav" role="navigation">
          <?php print $second_level_nav; ?>
        </nav>
      <?php } ?>
    </div><!--/#main-->
  </div>

  <footer role="contentinfo">
    <div class="footer-site">
      <nav class="footer-primary-nav">
        <?php if (isset($footer_nav)) { print $footer_nav; } ?>
      </nav>
      <div class="department">
        <div class="site-name"><!-- Department of<wbr> --> <?php print $site_name; ?></div>
        <div class="location">
               <div class="address"><p>NewYork-Presbyterian Hospital/<br/>
        Weill Cornell Medical College<br/>
        525 East 68th Street, Box 124<br/>
        New York, NY 10065 <br/><br/>
        
        <?php if (!empty($site_fax)) { ?>
            Fax: <?php print $site_fax; ?>
          <?php } ?></p>
         </div>
        </div>
      </div>
      <div class="contact">
        <div class="phone">
          <p><strong>Office of the Chair</strong><br/>
      Phone: (212) 746-2962<br/>
      E-mail:  Marissa Matarazzo, <br/>
      <a href="mailto:mam7036@med.cornell.edu">mam7036@med.cornell.edu</a></p>
     
     <p><strong>Residency and Fellowship Education</strong><br />
Direct all inquiries to:<br />
Phone: (212) 746-2941<br />
<a href="mailto:anes-programs@med.cornell.edu">anes-programs@med.cornell.edu</a></p>
     
      <p><strong>Patient Billing Office</strong><br/>
      Phone: (855) 880-0343</p>
        </div>
      </div>
    <div class="footer-wcmc">
      <div class="wcmc-logo">Weill Cornell Medical College</div>
      <div class="nyp-logo">NewYork-Presbyterian Hospital</div>
      <div class="disclaimer">
       Unless otherwise noted, © Weill Cornell Medical College. <a href="http://weill.cornell.edu/legal/">Legal information</a>
      </div>
      <?php print render($page['footer']); ?>
    </div>
  </footer>
</div>






  
    




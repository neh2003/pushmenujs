<?php
 // These variables are set in wcmc_fieldable_panes_preprocess_fieldable_panels_pane()
?>

<div class="headshot">
  <?php if ($image != '') { ?>
    <img src="<?php print file_create_url($image['uri']); ?>" alt="<?php print $image['alt']; ?>" />
  <?php } else { ?>
    <img src="/<?php print $module_path; ?>/images/people-placeholder.png" alt="placholder" />
  <?php } ?>
  <div class="headshot-details">
    <p class="name"><?php print $name; ?></p>
    <p class="title"><?php print $prof_title_display; ?></p>
    <div class="description"><?php print $description; ?></div>
  </div>
</div>
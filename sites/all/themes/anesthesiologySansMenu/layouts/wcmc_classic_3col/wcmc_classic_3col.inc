<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('WCMC Classic Three Column'),
  'category' => t('WCMC'),
  'icon' => 'wcmc-classic-3col.png',
  'theme' => 'wcmc_classic_3col',
  'theme arguments' => array('id', 'content'),
  'css' => 'wcmc_classic_3col.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left Column'),
    'center' => t('Center Column'),
    'right' => t('Right Column'),
  ),
);


<?php

/**
 * implementation of hook_panels_layouts
 */
// Plugin definition
$plugin = array(
  'title' => t('WCMC Intranet Home'),
  'category' => t('WCMC'),
  'icon' => 'wcmc_intra_home.png',
  'theme' => 'wcmc_intra_home',
  'theme arguments' => array('id', 'content'),
  'css' => 'wcmc_intra_home.css',
  'regions' => array(
    'header' => t('Header'),
    'left' => t('Left Column'),
    'center' => t('Center Column'),
    'right' => t('Right Column'),
  ),
);


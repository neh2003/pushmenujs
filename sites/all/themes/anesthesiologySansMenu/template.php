<?php

/**
 * Change default separator in the breadcrumb
 */
function anesthesiologySansMenu_breadcrumb($variables) {
  $breadcrumb = $variables['breadcrumb'];
  if (!empty($breadcrumb)) {
    // Provide a navigational heading to give context for breadcrumb links to
    // screen-reader users. Make the heading invisible with .element-invisible.
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode('&nbsp; / &nbsp;', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Perform alterations to the JavaScript before it is presented on the page.
 */
function anesthesiologySansMenu_js_alter(&$javascript) {
  // Typekit - Remove the Base Theme font kit
  unset($javascript['//use.typekit.net/icu8dxt.js']);
}

/**
 * Override or insert variables into the html templates.
 */
function anesthesiologySansMenu_preprocess_html(&$variables, $hook) {
  // Typekit - Add sub-theme font kit
  drupal_add_js('//use.typekit.net/aqc5rek.js', 'external');
  // drupal_add_css('http://fonts.googleapis.com/css?family=Open+Sans+Condensed:300,300italic,700&subset=latin,cyrillic-ext,latin-ext,cyrillic', array('type' => 'external'));
  // drupal_add_css('http://netdna.bootstrapcdn.com/font-awesome/4.0.1/css/font-awesome.min.css', array('type' => 'external'));
}


/**
 * Override or insert variables into the page template.
 */
function anesthesiologySansMenu_preprocess_page(&$variables, $hook) {
  if ($variables['is_front']) {
    drupal_add_js(drupal_get_path('theme','anesthesiologySansMenu') . '/js/jquery.customSelect.min.js');
    drupal_add_js('
    jQuery(document).ready(function() {
      jQuery("#fellowships").customSelect().show();
      jQuery("#fellowships").change(function(){
         window.location.href = this.value;
      })
    });
    ',
    array('type' => 'inline')
    );
  }
}



// function anesthesiologySansMenu_menu_link__menu_block__main_menu(array $variables) {
//   $element = $variables['element'];
//   $sub_menu = '';

//   // Get the menu item's parent and save it to be used in the theme_menu_tree
//   $parent = $element['#original_link']['plid'];

//   if ($parent != '0') {
//     $parent_item = menu_link_load($parent);
//     $element['#attributes']['data-menu-parent'] = $parent_item['link_title'];
//   }


//   // Check if there is text in the menu item description, if there is save it to a var.
//   if ((isset($element['#localized_options']['attributes']['title'])) &&
//     $element['#localized_options']['attributes']['title'] != '') {
//       $menu_link = l($element['#localized_options']['attributes']['title'], $element['#href'], $element['#localized_options']);
//       $description = '<p class="menu-description">' . $menu_link . '</p>';
//   }

//   if ($element['#below']) {
//     $sub_menu = drupal_render($element['#below']);
//   }

//   // Add a button class to the hrefs in our quirky level two menu, only on level one pages
//   if ($element['#original_link']['depth'] == 2 && $element['#bid']['delta'] == 'second_level_nav') {
//     $element['#localized_options']['attributes']['class'][] = 'wcmc-button';
//   }

//   // If there is a description for a menu item, and it is the top level of the drawer, display it underneath
//   if ($element['#original_link']['depth'] == 1 && $element['#bid']['delta'] == 'drawer_nav'
//       && isset($element['#localized_options']['attributes']['title'])
//       && $element['#localized_options']['attributes']['title'] != '') {
//     $output = l($element['#title'], $element['#href'], $element['#localized_options']) . $description;
//   } else {
//     $output = l($element['#title'], $element['#href'], $element['#localized_options']);
//   }

//   // Clean up default classes
//   // $remove = array('leaf','collapsed','expanded','expandable', 'has-children');
//   // $element['#attributes']['class'] = array_diff($element['#attributes']['class'] , $remove);

//   // Add a class to note depth
//   $element['#attributes']['class'][] = 'level-' . $element['#original_link']['depth'];

//   // Add spans before and after the link just for level one items.
//   if ($element['#original_link']['depth'] == 1 && $element['#bid']['delta'] != 'primary_nav' && $element['#bid']['delta'] != 'footer_nav') {
//     return '<li' . drupal_attributes($element['#attributes']) . '> ' . $output . /*'<span class="expand-menu">+</span>' .*/ $sub_menu . "</li>\n";
//   } else {
//     return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";
//   }
// }

// function anesthesiologySansMenu_menu_tree__menu_block__main_menu($variables) {
//   libxml_use_internal_errors(true);
//   $tree = new DOMDocument();
//   $tree->loadHTML($variables['tree']);
//   $links = $tree->getElementsByTagname('li');
//   $clicks = $tree->getElementsByTagname('a');
//   $link = $links->item(0);
//   $click = $clicks->item(0);

//   $parent = $link->getAttribute('data-menu-parent');
//   $plink = $click->getAttribute('href');

//   $variables['parent_name'] = $parent;
//   $variables['parent_link'] = $plink;

//   //Count number of menu items
//   $num_top_level_items = count(menu_tree_page_data('main-menu'));

//   $output = " ";
//   if($variables['parent_name'] != ""){
//     $output = '<h2><i class="fa fa-reorder"></i><a href="'.$variables['parent_link'].'"/>' . $variables['parent_name'] . '</a></h2>';
//   }

//   $output .= '<ul class="thistree" data-li-count="' . $num_top_level_items .'">' . $variables['tree'] . '</ul>';
//   return $output;
// }






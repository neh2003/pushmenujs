<!DOCTYPE html>

<!--[if IE 7]>    <html class="lt-ie9 lt-ie8 no-js" <?php print $html_attributes; ?>> <![endif]-->
<!--[if IE 8]>    <html class="lt-ie9 ie8 no-js" <?php print $html_attributes; ?>> <![endif]-->
<!--[if gt IE 8]><!--> <html class="no-js" <?php print $html_attributes; ?>> <!--<![endif]-->

  <head>
    <?php print $head; ?>
    <title><?php print $head_title; ?></title>
    <!-- http://t.co/dKP3o1e -->
    <meta name="HandheldFriendly" content="True">
    <meta name="MobileOptimized" content="320">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <?php print $styles; ?>
    <link rel="stylesheet" type="text/css" href=
    <link rel="stylesheet" type="text/css" href="http://local.layered.com/sites/all/modules/layered_push_menu/css/component.css"> 
    <?php print $scripts; ?>
   
  </head>
  <body class="<?php print $classes; ?>" <?php print $attributes; ?>>
    <?php print $page_top; ?>    
    <?php print $page; ?>        
    <?php print $page_bottom; ?> 
    <script type="text/javascript" src='http://local.layered.com/sites/all/modules/layered_push_menu/js/modernizr.custom.js'></script>
    <script src='http://local.layered.com/sites/all/modules/layered_push_menu/js/classie.js' type="text/javascript"></script>
    <script src='http://local.layered.com/sites/all/modules/layered_push_menu/js/mlpushmenu.js' type='text/javascript' ></script>
    <script type="text/javascript">
      new mlPushMenu( document.getElementById( "mp-menu" ), document.getElementById( "trigger" ) );
    </script>
  </body>
</html>